package models

func (ms *ModelSuite) Test_User_Create() {
	ms.LoadFixture("load user types")
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	ut := UserType{}
	err = ms.DB.First(&ut)
	ms.NoError(err)

	u := &User{
		Email:                "mark@example.com",
		Password:             "password",
		PasswordConfirmation: "password",
		UserTypeID:           ut.ID,
	}
	ms.Zero(u.PasswordHash)

	verrs, err := u.Create(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NotZero(u.PasswordHash)

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)
}

func (ms *ModelSuite) Test_User_Create_ValidationErrors() {
	ms.LoadFixture("load user types")
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	u := &User{
		Password: "password",
	}
	ms.Zero(u.PasswordHash)

	verrs, err := u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)
}

func (ms *ModelSuite) Test_User_Create_UserExists() {
	ms.LoadFixture("load user types")
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)
	ut := UserType{}
	err = ms.DB.First(&ut)
	ms.NoError(err)

	u := &User{
		Email:                "mark@example.com",
		Password:             "password",
		PasswordConfirmation: "password",
		UserTypeID:           ut.ID,
	}
	ms.Zero(u.PasswordHash)

	verrs, err := u.Create(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())
	ms.NotZero(u.PasswordHash)

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)

	u = &User{
		Email:      "mark@example.com",
		Password:   "password",
		UserTypeID: ut.ID,
	}
	verrs, err = u.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	count, err = ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(1, count)
}
