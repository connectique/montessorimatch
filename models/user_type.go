package models

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/validate"
	"github.com/gobuffalo/validate/validators"
	"github.com/gofrs/uuid"
)

type UserType struct {
	ID        uuid.UUID `json:"id" db:"id"`
	CreatedAt time.Time `json:"createdAt" db:"created_at"`
	UpdatedAt time.Time `json:"updatedAt" db:"updated_at"`
	Name      string    `json:"name" db:"name"`
}

// String is not required by pop and may be deleted
func (u UserType) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// UserTypes is not required by pop and may be deleted
type UserTypes []UserType

// String is not required by pop and may be deleted
func (u UserTypes) String() string {
	ju, _ := json.Marshal(u)
	return string(ju)
}

// Create wraps up the pattern of encrypting the password and
// running validations. Useful when writing tests.
func (ut *UserType) Create(tx *pop.Connection) (*validate.Errors, error) {
	ut.Name = strings.ToLower(ut.Name)
	return tx.ValidateAndCreate(ut)
}

// Validate gets run every time you call a "pop.Validate*" (pop.ValidateAndSave, pop.ValidateAndCreate, pop.ValidateAndUpdate) method.
// This method is not required and may be deleted.
func (ut *UserType) Validate(tx *pop.Connection) (*validate.Errors, error) {
	var err error
	return validate.Validate(
		&validators.StringIsPresent{Field: ut.Name, Name: "Name"},
		// check to see if the name is already taken:
		&validators.FuncValidator{
			Field:   ut.Name,
			Name:    "Name",
			Message: "%s is already taken",
			Fn: func() bool {
				var b bool
				q := tx.Where("name = ?", ut.Name)
				if ut.ID != uuid.Nil {
					q = q.Where("id != ?", ut.ID)
				}
				b, err = q.Exists(ut)
				if err != nil {
					return false
				}
				return !b
			},
		},
	), nil
}

// ValidateCreate gets run every time you call "pop.ValidateAndCreate" method.
// This method is not required and may be deleted.
func (u *UserType) ValidateCreate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}

// ValidateUpdate gets run every time you call "pop.ValidateAndUpdate" method.
// This method is not required and may be deleted.
func (u *UserType) ValidateUpdate(tx *pop.Connection) (*validate.Errors, error) {
	return validate.NewErrors(), nil
}
