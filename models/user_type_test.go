package models

func (ms *ModelSuite) Test_UserType_Create() {
	count, err := ms.DB.Count("users")
	ms.NoError(err)
	ms.Equal(0, count)

	ut := &UserType{
		Name: "educator",
	}

	verrs, err := ut.Create(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	count, err = ms.DB.Count("user_types")
	ms.NoError(err)
	ms.Equal(1, count)
}

func (ms *ModelSuite) Test_UserType_Create_ValidationErrors() {
	count, err := ms.DB.Count("user_types")
	ms.NoError(err)
	ms.Equal(0, count)

	ut := &UserType{
		Name: "",
	}

	verrs, err := ut.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	count, err = ms.DB.Count("user_types")
	ms.NoError(err)
	ms.Equal(0, count)
}

func (ms *ModelSuite) Test_UserType_Create_UserExists() {
	count, err := ms.DB.Count("user_types")
	ms.NoError(err)
	ms.Equal(0, count)

	ut := &UserType{
		Name: "educator",
	}

	verrs, err := ut.Create(ms.DB)
	ms.NoError(err)
	ms.False(verrs.HasAny())

	count, err = ms.DB.Count("user_types")
	ms.NoError(err)
	ms.Equal(1, count)

	ut = &UserType{
		Name: "educator",
	}
	verrs, err = ut.Create(ms.DB)
	ms.NoError(err)
	ms.True(verrs.HasAny())

	count, err = ms.DB.Count("user_types")
	ms.NoError(err)
	ms.Equal(1, count)
}
