package actions

import (
	"gitlab.com/connectique/montessorimatch/models"
)

func (as *ActionSuite) Test_Users_New() {
	res := as.HTML("/users/new").Get()
	as.Equal(200, res.Code)
}

func (as *ActionSuite) Test_Users_Create() {
	as.LoadFixture("load user types")
	count, err := as.DB.Count("users")
	as.NoError(err)
	as.Equal(0, count)

	ut := models.UserType{}
	err = as.DB.First(&ut)
	as.NoError(err)

	u := &models.User{
		Email:                "mark@example.com",
		Password:             "password",
		PasswordConfirmation: "password",
		UserTypeID:           ut.ID,
	}

	res := as.HTML("/users").Post(u)
	as.Equal(302, res.Code)

	count, err = as.DB.Count("users")
	as.NoError(err)
	as.Equal(1, count)
}
