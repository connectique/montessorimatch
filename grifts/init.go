package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/connectique/montessorimatch/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
